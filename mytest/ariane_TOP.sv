
//import my_soc::*;

module ariane_TOP #(
		parameter int unsigned AXI_USER_WIDTH    = 1,
		parameter int unsigned AXI_ADDRESS_WIDTH = 64,
		parameter int unsigned AXI_DATA_WIDTH    = 64,
		parameter int unsigned NUM_WORDS         = 2**25,         // memory size
		parameter bit          StallRandomOutput = 1'b0,
		parameter bit          StallRandomInput  = 1'b0
	) (
		input  logic                           clk_i,
		input  logic                           rtc_i,
		input  logic                           rst_ni,
		output logic [31:0]                    exit_o
	);

	// disable test-enable
	logic        ndmreset;
	logic        ndmreset_n;

	assign ndmreset = 1'b0;
	assign exit_o = 1'b0;

	AXI_BUS #(
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH   ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH      ),
		.AXI_ID_WIDTH   ( my_soc::IdWidth ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH      )
	) cores[my_soc::NrCores-1:0]();

	AXI_BUS #(
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH        ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH           ),
		.AXI_ID_WIDTH   ( my_soc::IdWidthSlave     ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH           )
	) master[my_soc::NB_PERIPHERALS-1:0]();

	rstgen i_rstgen_main (
		.clk_i        ( clk_i                ),
		.rst_ni       ( rst_ni & (~ndmreset) ),
		.test_mode_i  ( 1'b0                 ),
		.rst_no       ( ndmreset_n           ),
		.init_no      (                      ) // keep open
	);

	// ---------------
	// ROM
	// ---------------
	logic                         rom_req;
	logic [AXI_ADDRESS_WIDTH-1:0] rom_addr;
	logic [AXI_DATA_WIDTH-1:0]    rom_rdata;

	axi2mem #(
		.AXI_ID_WIDTH   ( 4 ),
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH        ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH           ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH           )
	) i_axi2rom (
		.clk_i  ( clk_i                   ),
		.rst_ni ( ndmreset_n              ),
		.slave  ( master[my_soc::ROM]     ),
		.req_o  ( rom_req                 ),
		.we_o   (                         ),
		.addr_o ( rom_addr                ),
		.be_o   (                         ),
		.data_o (                         ),
		.data_i ( rom_rdata               )
	);

	bootrom i_bootrom (
		.clk_i      ( clk_i     ),
		.req_i      ( rom_req   ),
		.addr_i     ( rom_addr  ),
		.rdata_o    ( rom_rdata )
	);

	// ------------------------------
	// Memory + Exclusive Access
	// ------------------------------
	AXI_BUS #(
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH        ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH           ),
		.AXI_ID_WIDTH   ( my_soc::IdWidthSlave     ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH           )
	) dram();

	logic                         req;
	logic                         we;
	logic [AXI_ADDRESS_WIDTH-1:0] addr;
	logic [AXI_DATA_WIDTH/8-1:0]  be;
	logic [AXI_DATA_WIDTH-1:0]    wdata;
	logic [AXI_DATA_WIDTH-1:0]    rdata;

	axi_riscv_atomics_wrap #(
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH        ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH           ),
		.AXI_ID_WIDTH   ( my_soc::IdWidthSlave     ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH           ),
		.AXI_MAX_WRITE_TXNS ( 1  ),
		.RISCV_WORD_WIDTH   ( 64 )
	) i_axi_riscv_atomics (
		.clk_i,
		.rst_ni ( ndmreset_n               ),
		.slv    ( master[my_soc::DRAM] ),
		.mst    ( dram                     )
	);

	AXI_BUS #(
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH        ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH           ),
		.AXI_ID_WIDTH   ( my_soc::IdWidthSlave ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH           )
	) dram_delayed();

	ariane_axi::aw_chan_slv_t aw_chan_i;
	ariane_axi::w_chan_t      w_chan_i;
	ariane_axi::b_chan_slv_t  b_chan_o;
	ariane_axi::ar_chan_slv_t ar_chan_i;
	ariane_axi::r_chan_slv_t  r_chan_o;
	ariane_axi::aw_chan_slv_t aw_chan_o;
	ariane_axi::w_chan_t      w_chan_o;
	ariane_axi::b_chan_slv_t  b_chan_i;
	ariane_axi::ar_chan_slv_t ar_chan_o;
	ariane_axi::r_chan_slv_t  r_chan_i;

	axi_delayer #(
		.aw_t              ( ariane_axi::aw_chan_slv_t ),
		.w_t               ( ariane_axi::w_chan_t      ),
		.b_t               ( ariane_axi::b_chan_slv_t  ),
		.ar_t              ( ariane_axi::ar_chan_slv_t ),
		.r_t               ( ariane_axi::r_chan_slv_t  ),
		.StallRandomOutput ( StallRandomOutput         ),
		.StallRandomInput  ( StallRandomInput          ),
		.FixedDelayInput   ( 0                         ),
		.FixedDelayOutput  ( 0                         )
	) i_axi_delayer (
		.clk_i      ( clk_i                 ),
		.rst_ni     ( ndmreset_n            ),
		.aw_valid_i ( dram.aw_valid         ),
		.aw_chan_i  ( aw_chan_i             ),
		.aw_ready_o ( dram.aw_ready         ),
		.w_valid_i  ( dram.w_valid          ),
		.w_chan_i   ( w_chan_i              ),
		.w_ready_o  ( dram.w_ready          ),
		.b_valid_o  ( dram.b_valid          ),
		.b_chan_o   ( b_chan_o              ),
		.b_ready_i  ( dram.b_ready          ),
		.ar_valid_i ( dram.ar_valid         ),
		.ar_chan_i  ( ar_chan_i             ),
		.ar_ready_o ( dram.ar_ready         ),
		.r_valid_o  ( dram.r_valid          ),
		.r_chan_o   ( r_chan_o              ),
		.r_ready_i  ( dram.r_ready          ),
		.aw_valid_o ( dram_delayed.aw_valid ),
		.aw_chan_o  ( aw_chan_o             ),
		.aw_ready_i ( dram_delayed.aw_ready ),
		.w_valid_o  ( dram_delayed.w_valid  ),
		.w_chan_o   ( w_chan_o              ),
		.w_ready_i  ( dram_delayed.w_ready  ),
		.b_valid_i  ( dram_delayed.b_valid  ),
		.b_chan_i   ( b_chan_i              ),
		.b_ready_o  ( dram_delayed.b_ready  ),
		.ar_valid_o ( dram_delayed.ar_valid ),
		.ar_chan_o  ( ar_chan_o             ),
		.ar_ready_i ( dram_delayed.ar_ready ),
		.r_valid_i  ( dram_delayed.r_valid  ),
		.r_chan_i   ( r_chan_i              ),
		.r_ready_o  ( dram_delayed.r_ready  )
	);

	assign aw_chan_i.atop = dram.aw_atop;
	assign aw_chan_i.id = dram.aw_id;
	assign aw_chan_i.addr = dram.aw_addr;
	assign aw_chan_i.len = dram.aw_len;
	assign aw_chan_i.size = dram.aw_size;
	assign aw_chan_i.burst = dram.aw_burst;
	assign aw_chan_i.lock = dram.aw_lock;
	assign aw_chan_i.cache = dram.aw_cache;
	assign aw_chan_i.prot = dram.aw_prot;
	assign aw_chan_i.qos = dram.aw_qos;
	assign aw_chan_i.region = dram.aw_region;

	assign ar_chan_i.id = dram.ar_id;
	assign ar_chan_i.addr = dram.ar_addr;
	assign ar_chan_i.len = dram.ar_len;
	assign ar_chan_i.size = dram.ar_size;
	assign ar_chan_i.burst = dram.ar_burst;
	assign ar_chan_i.lock = dram.ar_lock;
	assign ar_chan_i.cache = dram.ar_cache;
	assign ar_chan_i.prot = dram.ar_prot;
	assign ar_chan_i.qos = dram.ar_qos;
	assign ar_chan_i.region = dram.ar_region;

	assign w_chan_i.data = dram.w_data;
	assign w_chan_i.strb = dram.w_strb;
	assign w_chan_i.last = dram.w_last;

	assign dram.r_id = r_chan_o.id;
	assign dram.r_data = r_chan_o.data;
	assign dram.r_resp = r_chan_o.resp;
	assign dram.r_last = r_chan_o.last;

	assign dram.b_id = b_chan_o.id;
	assign dram.b_resp = b_chan_o.resp;

	assign dram_delayed.aw_id = aw_chan_o.id;
	assign dram_delayed.aw_addr = aw_chan_o.addr;
	assign dram_delayed.aw_len = aw_chan_o.len;
	assign dram_delayed.aw_size = aw_chan_o.size;
	assign dram_delayed.aw_burst = aw_chan_o.burst;
	assign dram_delayed.aw_lock = aw_chan_o.lock;
	assign dram_delayed.aw_cache = aw_chan_o.cache;
	assign dram_delayed.aw_prot = aw_chan_o.prot;
	assign dram_delayed.aw_qos = aw_chan_o.qos;
	assign dram_delayed.aw_atop = aw_chan_o.atop;
	assign dram_delayed.aw_region = aw_chan_o.region;
	assign dram_delayed.aw_user = '0;

	assign dram_delayed.ar_id = ar_chan_o.id;
	assign dram_delayed.ar_addr = ar_chan_o.addr;
	assign dram_delayed.ar_len = ar_chan_o.len;
	assign dram_delayed.ar_size = ar_chan_o.size;
	assign dram_delayed.ar_burst = ar_chan_o.burst;
	assign dram_delayed.ar_lock = ar_chan_o.lock;
	assign dram_delayed.ar_cache = ar_chan_o.cache;
	assign dram_delayed.ar_prot = ar_chan_o.prot;
	assign dram_delayed.ar_qos = ar_chan_o.qos;
	assign dram_delayed.ar_region = ar_chan_o.region;
	assign dram_delayed.ar_user = '0;

	assign dram_delayed.w_data = w_chan_o.data;
	assign dram_delayed.w_strb = w_chan_o.strb;
	assign dram_delayed.w_last = w_chan_o.last;
	assign dram_delayed.w_user = '0;

	assign r_chan_i.id = dram_delayed.r_id;
	assign r_chan_i.data = dram_delayed.r_data;
	assign r_chan_i.resp = dram_delayed.r_resp;
	assign r_chan_i.last = dram_delayed.r_last;
	assign dram.r_user = '0;

	assign b_chan_i.id = dram_delayed.b_id;
	assign b_chan_i.resp = dram_delayed.b_resp;
	assign dram.b_user = '0;

	axi2mem #(
		.AXI_ID_WIDTH   ( my_soc::IdWidthSlave     ),
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH        ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH           ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH           )
	) i_axi2mem (
		.clk_i  ( clk_i        ),
		.rst_ni ( ndmreset_n   ),
		.slave  ( dram_delayed ),
		.req_o  ( req          ),
		.we_o   ( we           ),
		.addr_o ( addr         ),
		.be_o   ( be           ),
		.data_o ( wdata        ),
		.data_i ( rdata        )
	);

	sram #(
		.DATA_WIDTH ( AXI_DATA_WIDTH ),
		.NUM_WORDS  ( NUM_WORDS      )
	) i_sram (
		.clk_i      ( clk_i                                                                       ),
		.rst_ni     ( rst_ni                                                                      ),
		.req_i      ( req                                                                         ),
		.we_i       ( we                                                                          ),
		.addr_i     ( addr[$clog2(NUM_WORDS)-1+$clog2(AXI_DATA_WIDTH/8):$clog2(AXI_DATA_WIDTH/8)] ),
		.wdata_i    ( wdata                                                                       ),
		.be_i       ( be                                                                          ),
		.rdata_o    ( rdata                                                                       )
	);

	// ---------------
	// Core 0
	// ---------------
	ariane_axi::req_t    axi_ariane_req0;
	ariane_axi::resp_t   axi_ariane_resp0;

	ariane #(
		.AxiIdWidth    ( my_soc::IdWidth                             ),
		.SwapEndianess ( 0                                           ),
		.CachedAddrBeg ( my_soc::DRAMBase                            ),
		.CachedAddrEnd ( (my_soc::DRAMBase + my_soc::DRAMLength) ),
		.DmBaseAddress ( my_soc::DebugBase                           )
	) i_ariane0 (
		.clk_i                ( clk_i               ),
		.rst_ni               ( ndmreset_n          ),
		.boot_addr_i          ( my_soc::ROMBase ), // start fetching from ROM
		.hart_id_i            ( '0                  ),
		.irq_i                ( 2'b00               ),
		.ipi_i                ( 1'b0                ),
		.time_irq_i           ( 1'b0                ),
		.debug_req_i          ( 1'b0                ),
		.axi_req_o            ( axi_ariane_req0     ),
		.axi_resp_i           ( axi_ariane_resp0    )
	);

	axi_master_connect i_axi_master_connect_ariane0 (
		.axi_req_i(axi_ariane_req0),
		.axi_resp_o(axi_ariane_resp0),
		.master(cores[0])
	);

	// ---------------
	// Core 1
	// ---------------
	ariane_axi::req_t    axi_ariane_req1;
	ariane_axi::resp_t   axi_ariane_resp1;

	ariane #(
		.AxiIdWidth    ( my_soc::IdWidth                             ),
		.SwapEndianess ( 0                                           ),
		.CachedAddrBeg ( my_soc::DRAMBase                            ),
		.CachedAddrEnd ( (my_soc::DRAMBase + my_soc::DRAMLength) ),
		.DmBaseAddress ( my_soc::DebugBase                           )
	) i_ariane1 (
		.clk_i                ( clk_i               ),
		.rst_ni               ( ndmreset_n          ),
		.boot_addr_i          ( my_soc::ROMBase     ), // start fetching from ROM
		.hart_id_i            ( 64'x0000000000000001),
		.irq_i                ( 2'b00               ),
		.ipi_i                ( 1'b0                ),
		.time_irq_i           ( 1'b0                ),
		.debug_req_i          ( 1'b0                ),
		.axi_req_o            ( axi_ariane_req1      ),
		.axi_resp_i           ( axi_ariane_resp1     )
	);

	axi_master_connect i_axi_master_connect_ariane1 (
		.axi_req_i(axi_ariane_req1),
		.axi_resp_o(axi_ariane_resp1),
		.master(cores[1])
	);


	// ---------------
	// AXI Xbar
	// ---------------
	/*axi_node_intf_wrap #(
		.NB_SLAVE           ( my_soc::NrCores            ),
		.NB_MASTER          ( my_soc::NB_PERIPHERALS     ),
		.NB_REGION          ( my_soc::NrRegion           ),
		.AXI_ADDR_WIDTH     ( AXI_ADDRESS_WIDTH          ),
		.AXI_DATA_WIDTH     ( AXI_DATA_WIDTH             ),
		.AXI_USER_WIDTH     ( AXI_USER_WIDTH             ),
		.AXI_ID_WIDTH       ( my_soc::IdWidth            )
	// .MASTER_SLICE_DEPTH ( 0                          ),
	// .SLAVE_SLICE_DEPTH  ( 0                          )
	) i_axi_xbar (
		.clk          ( clk_i      ),
		.rst_n        ( ndmreset_n ),
		.test_en_i    ( 1'b0       ),
		.slave        ( cores      ),
		.master       ( master     ),
		.start_addr_i ({
				my_soc::DRAMBase,
				my_soc::ROMBase
			}),
		.end_addr_i   ({
				my_soc::DRAMBase     + my_soc::DRAMLength - 1,
				my_soc::ROMBase      + my_soc::ROMLength - 1
			}),
		.valid_rule_i (my_soc::ValidRule)
	);*/

	cores_crossbar #(
		.AXI_ADDRESS_WIDTH (AXI_ADDRESS_WIDTH),
		.AXI_DATA_WIDTH (AXI_DATA_WIDTH)
	) i_axi_simple_xbar (
		.clk (clk_i),
		.rst_n (ndmreset_n),
		.slaves(cores),
		.masters(master),
		.regions_low('{
				my_soc::ROMBase,
				my_soc::DRAMBase
			}),
		.regions_high('{
				my_soc::ROMBase      + my_soc::ROMLength - 1,
				my_soc::DRAMBase     + my_soc::DRAMLength - 1
			})
	);

	// -------------
	// Simulation Helper Functions
	// -------------
	// check for response errors
	always_comb begin
		if (axi_ariane_req0.r_ready &&
				axi_ariane_resp0.r_valid &&
				axi_ariane_resp0.r.resp inside {axi_pkg::RESP_DECERR, axi_pkg::RESP_SLVERR}) begin
			$warning("R Response Errored");
		end
		if (axi_ariane_req0.b_ready &&
				axi_ariane_resp0.b_valid &&
				axi_ariane_resp0.b.resp inside {axi_pkg::RESP_DECERR, axi_pkg::RESP_SLVERR}) begin
			$warning("B Response Errored");
		end
	end

endmodule
