
module std_coherent_wtbcache_controller #(
		parameter logic [63:0] CACHE_START_ADDR = 64'h8000_0000
	)(
		input  logic                                clk_i,       // Clock
		input  logic                                rst_ni,      // Asynchronous reset active low
		input  logic                                enable_i,    // from CSR
		input  logic                                flush_i,     // high until acknowledged
		output logic                                flush_ack_o, // send a single cycle acknowledge signal when the cache is flushed
		output logic                                miss_o,      // we missed on a LD/ST
		input  dcache_req_i_t                       req_port_i,  // request ports
		output dcache_req_o_t                       req_port_o,   // request ports
		output logic [DCACHE_SET_ASSOC-1:0]         req_ram,
		output logic [DCACHE_INDEX_WIDTH-1:0]       addr_ram,
		output logic                                we_ram,
		output cache_line_t                         wdata_ram,
		input cache_line_t [DCACHE_SET_ASSOC-1:0]   rdata_ram,
		output cl_be_t                              be_ram,
		output ariane_axi::req_t                    axi_data_o,
		input  ariane_axi::resp_t                   axi_data_i
	);

	typedef enum {
		IDLE,
		CHECK_MISS,
		WAIT_WRITE,
		WAIT_LINE,
		SAVE_CACHELINE,
		MISS_REPL
	} state_t;

	state_t state_q, state_d;
	logic [DCACHE_SET_ASSOC-1:0] hit_way;
	logic [DCACHE_SET_ASSOC-1:0] valid_way;


	logic [DCACHE_SET_ASSOC-1:0]    evict_way_q, evict_way_d;
	cache_line_t                            evict_cl_d, evict_cl_q;

	// Cache Line Refill <-> AXI
	logic                                    req_fsm_miss_valid;
	logic [63:0]                             req_fsm_miss_addr;
	logic [DCACHE_LINE_WIDTH-1:0]            req_fsm_miss_wdata;
	logic                                    req_fsm_miss_we;
	logic [(DCACHE_LINE_WIDTH/8)-1:0]        req_fsm_miss_be;
	ariane_axi::ad_req_t                     req_fsm_miss_req;
	logic [1:0]                              req_fsm_miss_size;

	logic                                    gnt_miss_fsm;
	logic                                    valid_miss_fsm;
	logic [(DCACHE_LINE_WIDTH/64)-1:0][63:0] data_miss_fsm;

	logic [63:0]                                 critical_word_o;
	logic                                        critical_word_valid_o;

	for (genvar j = 0; j < DCACHE_SET_ASSOC; j++) begin
		assign valid_way[j] = rdata_ram[j].valid;
	end

	for (genvar j = 0; j < DCACHE_SET_ASSOC; j++) begin : tag_cmp
		assign hit_way[j] = (req_port_i.address_tag == rdata_ram[j].tag) ? rdata_ram[j].valid : 1'b0;
	end

	logic [DCACHE_LINE_WIDTH-1:0] cl_i;

	always_comb begin : way_select
		cl_i = '0;
		for (int unsigned i = 0; i < DCACHE_SET_ASSOC; i++)
			if (hit_way[i])
				cl_i = rdata_ram[i].data;
	end

	always_ff @(posedge clk_i) begin: flip_flops
		if (~rst_ni) begin
			state_q = IDLE;
		end else begin
			state_q = state_d;
		end
	end

	always_comb begin : controller_logic
		automatic logic [$clog2(DCACHE_LINE_WIDTH)-1:0] cl_offset;
		// incoming cache-line -> this is needed as synthesis is not supporting +: indexing in a multi-dimensional array
		// cache-line offset -> multiple of 64
		cl_offset = req_port_i.address_index[DCACHE_BYTE_OFFSET-1:3] << 6; // shift by 6 to the left
		req_ram = '0;
		req_port_o.data_gnt = 0;
		req_port_o.data_rvalid = 0;
		req_port_o.data_rdata  = '0;
		state_d = state_q;
		be_ram.data = '0;
		be_ram.tag = '0;
		be_ram.vldrty = '0;

		req_fsm_miss_valid  = 1'b0;
		req_fsm_miss_addr   = '0;
		req_fsm_miss_wdata  = '0;
		req_fsm_miss_we     = 1'b0;
		req_fsm_miss_be     = '0;
		req_fsm_miss_req    = ariane_axi::CACHE_LINE_REQ;
		req_fsm_miss_size   = 2'b11;

		case (state_q)

			IDLE: begin
				if (req_port_i.data_req) begin
					req_ram = '1;
					addr_ram = req_port_i.address_index;
					we_ram = 0;
					req_port_o.data_gnt = ~req_port_i.data_we;
					state_d = CHECK_MISS;
				end

				CHECK_MISS: begin
					//Hit!!
					if (|hit_way) begin
						//Read, we are done here
						if (!req_port_i.data_we) begin
							case (req_port_i.address_index[3])
								1'b0: req_port_o.data_rdata = cl_i[63:0];
								1'b1: req_port_o.data_rdata = cl_i[127:64];
							endcase

							req_port_o.data_rvalid = 1;
							state_d = IDLE;
						//Write, write to cache and memory
						end else begin
							req_ram      = hit_way;
							addr_ram     = req_port_i.address_index;
							we_ram       = 1'b1;

							be_ram.vldrty = hit_way;

							// set the correct byte enable
							be_ram.data[cl_offset>>3 +: 8]  = req_port_i.data_be;
							wdata_ram.data[cl_offset  +: 64] = req_port_i.data_wdata;
							// ~> change the state
							wdata_ram.dirty = 1'b1;
							wdata_ram.valid = 1'b1;
							state_d = WAIT_WRITE;
						end
					//Miss!!
					end else begin
						state_d = req_port_i.data_we?WAIT_WRITE:WAIT_LINE;
						miss_o = 1;
					end
				end

				WAIT_WRITE: begin
					req_fsm_miss_req = ariane_axi::SINGLE_REQ;
					req_fsm_miss_valid = 1;
					req_fsm_miss_addr = {req_port_i.address_tag, req_port_i.address_index & 12'b111111110000};
					req_fsm_miss_wdata = {{64{1'b0}}, req_port_i.data_wdata};
					req_fsm_miss_we = 1;
					req_fsm_miss_be = {{8{1'b0}}, req_port_i.data_be};
					if (gnt_miss_fsm) begin
						state_d = IDLE;
					end
				end

				WAIT_LINE: begin
					req_fsm_miss_req = ariane_axi::CACHE_LINE_REQ;
					req_fsm_miss_valid = 1;
					req_fsm_miss_addr = {req_port_i.address_tag, req_port_i.address_index & 12'b111111110000};
					req_fsm_miss_be = '1;
					if (gnt_miss_fsm) begin
						state_d = SAVE_CACHELINE;
					end
				end

				// ~> replace the cacheline
				SAVE_CACHELINE: begin
					// calculate cacheline offset
					automatic logic [$clog2(DCACHE_LINE_WIDTH)-1:0] cl_offset;
					cl_offset = req_port_i.address_index[DCACHE_BYTE_OFFSET-1:3] << 6;
					// we've got a valid response from refill unit
					if (valid_miss_fsm) begin

						if (&valid_way) begin
							be_ram.vldrty = lfsr_oh;
							req_ram = be_ram.vldrty;
						// we have at least one free way
						end else begin
							// get victim cache-line by looking for the first non-valid bit
							be_ram.vldrty = get_victim_cl(~valid_way);
							req_ram = be_ram.vldrty;
						end


						addr_ram       = req_port_i.address_index;
						we_ram         = 1'b1;

						be_ram         = '1;
						wdata_ram.tag   = req_port_i.address_tag;
						wdata_ram.data  = data_miss_fsm;
						wdata_ram.valid = 1'b1;
						wdata_ram.dirty = 1'b0;


						case (req_port_i.address_index[3])
							1'b0: req_port_o.data_rdata = data_miss_fsm[63:0];
							1'b1: req_port_o.data_rdata = data_miss_fsm[127:64];
						endcase

						req_port_o.data_rvalid = 1;

						// go back to idle
						state_d = IDLE;
					end
				end

				// ~> second miss cycle
				MISS_REPL: begin
				// if all are valid we need to evict one, pseudo random from LFSR

				end

			end
		endcase;
	end


	// ----------------------
	// Cache Line AXI Refill
	// ----------------------
	axi_adapter  #(
		.DATA_WIDTH            ( DCACHE_LINE_WIDTH  ),
		.AXI_ID_WIDTH          ( 4                  ),
		.CACHELINE_BYTE_OFFSET ( DCACHE_BYTE_OFFSET )
	) i_miss_axi_adapter (
		.clk_i,
		.rst_ni,
		.req_i               ( req_fsm_miss_valid ),
		.type_i              ( req_fsm_miss_req   ),
		.gnt_o               ( gnt_miss_fsm       ),
		.addr_i              ( req_fsm_miss_addr  ),
		.we_i                ( req_fsm_miss_we    ),
		.wdata_i             ( req_fsm_miss_wdata ),
		.be_i                ( req_fsm_miss_be    ),
		.size_i              ( req_fsm_miss_size  ),
		.id_i                ( 4'b1100            ),
		.gnt_id_o            (                    ), // open
		.valid_o             ( valid_miss_fsm     ),
		.rdata_o             ( data_miss_fsm      ),
		.id_o                (                    ),
		.critical_word_o,
		.critical_word_valid_o,
		.axi_req_o           ( axi_data_o         ),
		.axi_resp_i          ( axi_data_i         )
	);

	// -----------------
	// Replacement LFSR
	// -----------------
	lfsr_8bit #(.WIDTH (DCACHE_SET_ASSOC)) i_lfsr (
		.en_i           ( '1 ),
		.refill_way_oh  ( lfsr_oh     ),
		.refill_way_bin ( lfsr_bin    ),
		.*
	);

endmodule;
