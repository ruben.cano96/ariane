package my_soc;
	
	localparam NrCores = 2; //Dual core
	localparam NB_PERIPHERALS = 2; //Bootrom + DRAM
	
	localparam IdWidth = 4;
	localparam IdWidthSlave = IdWidth + $clog2(NrCores);
	
	localparam NrRegion = 1;
	
	typedef enum int unsigned {
        ROM      = 0,
        DRAM     = 1
	} axi_slaves_t;
	
    localparam logic[63:0] ROMLength      = 64'h10000;
    localparam logic[63:0] DRAMLength     = 64'h40000000; // 1GByte of DDR (split between two chips on Genesys2)
	
	typedef enum logic [63:0] {
		DebugBase    = 64'h0000_0000,
        ROMBase      = 64'h0001_0000,
        DRAMBase     = 64'h8000_0000
	} soc_bus_start_t;
	
	localparam logic [NrRegion-1:0][NB_PERIPHERALS-1:0] ValidRule = {{NrRegion * NB_PERIPHERALS}{1'b1}};
	
endpackage
