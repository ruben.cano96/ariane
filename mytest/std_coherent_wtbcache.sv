import ariane_pkg::*;
import std_cache_pkg::*;

module std_coherent_wtbcache #(
    parameter logic [63:0] CACHE_START_ADDR = 64'h8000_0000
)(
    input  logic                           clk_i,       // Clock
    input  logic                           rst_ni,      // Asynchronous reset active low
    // Cache management
    input  logic                           enable_i,    // from CSR
    input  logic                           flush_i,     // high until acknowledged
    output logic                           flush_ack_o, // send a single cycle acknowledge signal when the cache is flushed
    output logic                           miss_o,      // we missed on a LD/ST
    // AMOs
    input  amo_req_t                       amo_req_i,
    output amo_resp_t                      amo_resp_o,
    // Request ports
    input  dcache_req_i_t [2:0]            req_ports_i,  // request ports
    output dcache_req_o_t [2:0]            req_ports_o,  // request ports
    // Cache AXI refill port
    output ariane_axi::req_t               axi_data_o,
    input  ariane_axi::resp_t              axi_data_i,
    output ariane_axi::req_t               axi_bypass_o,
    input  ariane_axi::resp_t              axi_bypass_i
);
	
    logic [DCACHE_SET_ASSOC-1:0]         req_ram;
    logic [DCACHE_INDEX_WIDTH-1:0]       addr_ram;
    logic                                we_ram;
    cache_line_t                         wdata_ram;
    cache_line_t [DCACHE_SET_ASSOC-1:0]  rdata_ram;
    cl_be_t                              be_ram;
		
	typedef enum {
		IDLE,
		WAIT_FINISH
	} state_port_arb_t;
	
	logic [1:0] select_port;
	state_port_arb_t state_port_arb; //Arbiter to select request port
	dcache_req_i_t sel_req_port_i;
	dcache_req_o_t sel_req_port_o;
	
	for (genvar i = 0; i < 3; ++i) begin
		always_comb begin
			req_ports_o[i].data_rdata = sel_req_port_o.data_rdata;
		end
	end
	
	always_comb begin
		req_ports_o[0].data_rvalid = 0;
		req_ports_o[0].data_gnt = 0;
		req_ports_o[1].data_rvalid = 0;
		req_ports_o[1].data_gnt = 0;
		req_ports_o[2].data_rvalid = 0;
		req_ports_o[2].data_gnt = 0;
		case (select_port)
			2'b00: begin
				sel_req_port_i = req_ports_i[0];
				req_ports_o[0].data_rvalid = sel_req_port_o.data_rvalid;
				req_ports_o[0].data_gnt = sel_req_port_o.data_gnt;
			end
			2'b01: begin
				sel_req_port_i = req_ports_i[1];
				req_ports_o[1].data_rvalid = sel_req_port_o.data_rvalid;
				req_ports_o[1].data_gnt = sel_req_port_o.data_gnt;
			end
			2'b10: begin
				sel_req_port_i = req_ports_i[2];
				req_ports_o[2].data_rvalid = sel_req_port_o.data_rvalid;
				req_ports_o[2].data_gnt = sel_req_port_o.data_gnt;
			end
			default: begin
				sel_req_port_i.data_req = 0;
				sel_req_port_i.address_index = 0;
				sel_req_port_i.address_tag = 0;
				sel_req_port_i.data_be = 0;
				sel_req_port_i.data_size = 0;
				sel_req_port_i.data_wdata = 0;
				sel_req_port_i.data_we = 0;
				sel_req_port_i.kill_req = 0;
				sel_req_port_i.tag_valid = 0;
			end
		endcase;
	end
	
	//Arbiter logic
	always_ff @(posedge clk_i) begin
		if (~rst_ni) begin
			state_port_arb = IDLE;
			select_port = 2'b11; //Nothing selected
		end
		else begin
			case (state_port_arb)
				IDLE: begin
					if (req_ports_i[1].data_req) begin
						select_port = 2'b01;
						state_port_arb = WAIT_FINISH;
					end else if (req_ports_i[2].data_req) begin
						select_port = 2'b01;
						state_port_arb = WAIT_FINISH;
					end
				end
				
				WAIT_FINISH: begin
					if (sel_req_port_i.data_we && sel_req_port_o.data_gnt) begin
						state_port_arb = IDLE;
						select_port = 2'b11;
					end else if (!sel_req_port_i.data_we && sel_req_port_o.data_rvalid) begin
						state_port_arb = IDLE;
						select_port = 2'b11;
					end
				end
			endcase;
		end
	end

	// --------------
    // Memory Arrays
    // --------------
    for (genvar i = 0; i < DCACHE_SET_ASSOC; i++) begin : sram_block
        sram #(
            .DATA_WIDTH ( DCACHE_LINE_WIDTH                 ),
            .NUM_WORDS  ( DCACHE_NUM_WORDS                  )
        ) data_sram (
            .req_i   ( req_ram [i]                          ),
            .rst_ni  ( rst_ni                               ),
            .we_i    ( we_ram                               ),
            .addr_i  ( addr_ram[DCACHE_INDEX_WIDTH-1:DCACHE_BYTE_OFFSET]  ),
            .wdata_i ( wdata_ram.data                       ),
            .be_i    ( be_ram.data                          ),
            .rdata_o ( rdata_ram[i].data                    ),
            .*
        );

        sram #(
            .DATA_WIDTH ( DCACHE_TAG_WIDTH                  ),
            .NUM_WORDS  ( DCACHE_NUM_WORDS                  )
        ) tag_sram (
            .req_i   ( req_ram [i]                          ),
            .rst_ni  ( rst_ni                               ),
            .we_i    ( we_ram                               ),
            .addr_i  ( addr_ram[DCACHE_INDEX_WIDTH-1:DCACHE_BYTE_OFFSET]  ),
            .wdata_i ( wdata_ram.tag                        ),
            .be_i    ( be_ram.tag                           ),
            .rdata_o ( rdata_ram[i].tag                     ),
            .*
        );

    end

    // ----------------
    // Valid/Dirty Regs
    // ----------------

    // align each valid/dirty bit pair to a byte boundary in order to leverage byte enable signals.
    // note: if you have an SRAM that supports flat bit enables for your target technology,
    // you can use it here to save the extra 4x overhead introduced by this workaround.
    logic [4*DCACHE_DIRTY_WIDTH-1:0] dirty_wdata, dirty_rdata;

    for (genvar i = 0; i < DCACHE_SET_ASSOC; i++) begin
        assign dirty_wdata[8*i]   = wdata_ram.dirty;
        assign dirty_wdata[8*i+1] = wdata_ram.valid;
        assign rdata_ram[i].dirty = dirty_rdata[8*i];
        assign rdata_ram[i].valid = dirty_rdata[8*i+1];
    end

    sram #(
        .DATA_WIDTH ( 4*DCACHE_DIRTY_WIDTH             ),
        .NUM_WORDS  ( DCACHE_NUM_WORDS                 )
    ) valid_dirty_sram (
        .clk_i   ( clk_i                               ),
        .rst_ni  ( rst_ni                              ),
        .req_i   ( |req_ram                            ),
        .we_i    ( we_ram                              ),
        .addr_i  ( addr_ram[DCACHE_INDEX_WIDTH-1:DCACHE_BYTE_OFFSET] ),
        .wdata_i ( dirty_wdata                         ),
        .be_i    ( be_ram.vldrty                       ),
        .rdata_o ( dirty_rdata                         )
    );

endmodule;