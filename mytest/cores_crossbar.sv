

module cores_crossbar #(
		parameter int unsigned AXI_ADDRESS_WIDTH = 64,
		parameter int unsigned AXI_DATA_WIDTH    = 64,
		parameter int unsigned AXI_ID_WIDTH      =  4,
		parameter int unsigned AXI_USER_WIDTH    =  1
	) (
		input clk,
		input rst_n,
		input [AXI_ADDRESS_WIDTH-1:0] regions_low[0:1],
		input [AXI_ADDRESS_WIDTH-1:0] regions_high[0:1],
		AXI_BUS.Slave slaves[0:1],
		AXI_BUS.Master masters[0:1]
	);

	logic [0:0] selectSlave;  //Slave selected to do the transaction
	logic [0:0] selectMaster; //Master selected to do the transaction

	typedef enum {
		IDLE,
		READ,
		WRITE
	} transaction_state_t;

	transaction_state_t state;

	AXI_BUS #(
		.AXI_ADDR_WIDTH ( AXI_ADDRESS_WIDTH        ),
		.AXI_DATA_WIDTH ( AXI_DATA_WIDTH           ),
		.AXI_ID_WIDTH   ( AXI_ID_WIDTH+1           ),
		.AXI_USER_WIDTH ( AXI_USER_WIDTH           )
	) mux;

	generate
		genvar i;

		for (i = 0; i < 2; ++i) begin
			always_comb begin
				if (selectSlave == i) begin
					mux.ar_valid = slaves[i].ar_valid;
					mux.ar_addr = slaves[i].ar_addr;
					mux.ar_burst = slaves[i].ar_burst;
					mux.ar_cache = slaves[i].ar_cache;
					mux.ar_id = {i,slaves[i].ar_id};
					mux.ar_len = slaves[i].ar_len;
					mux.ar_lock = slaves[i].ar_lock;
					mux.ar_prot = slaves[i].ar_prot;
					mux.ar_qos = slaves[i].ar_qos;
					mux.ar_region = slaves[i].ar_region;
					mux.ar_size = slaves[i].ar_size;
					mux.ar_user = slaves[i].ar_user;

					mux.aw_valid = slaves[i].aw_valid;
					mux.aw_addr = slaves[i].aw_addr;
					mux.aw_burst = slaves[i].aw_burst;
					mux.aw_cache = slaves[i].aw_cache;
					mux.aw_id = {i, slaves[i].aw_id};
					mux.aw_len = slaves[i].aw_len;
					mux.aw_lock = slaves[i].aw_lock;
					mux.aw_prot = slaves[i].aw_prot;
					mux.aw_qos = slaves[i].aw_qos;
					mux.aw_region = slaves[i].aw_region;
					mux.aw_size = slaves[i].aw_size;
					mux.aw_user = slaves[i].aw_user;

					mux.w_data = slaves[i].w_data;
					mux.w_last = slaves[i].w_last;
					mux.w_strb = slaves[i].w_strb;
					mux.w_user = slaves[i].w_user;

					mux.r_ready = slaves[i].r_ready;
					mux.b_ready = slaves[i].b_ready;
					mux.w_valid = slaves[i].w_valid;
				end
			end
		end

		for (i = 0; i < 2; ++i) begin
			always_comb begin
				if (selectMaster == i) begin
					mux.ar_ready = masters[i].ar_ready;
					mux.aw_ready = masters[i].aw_ready;
					
					mux.r_data = masters[i].r_data;
					mux.r_id   = masters[i].r_id[AXI_ID_WIDTH-1:0];
					mux.r_last = masters[i].r_last;
					mux.r_resp = masters[i].r_resp;
					mux.r_user = masters[i].r_user;

					mux.b_id   = masters[i].b_id[AXI_ID_WIDTH-1:0];
					mux.b_resp = masters[i].b_resp;
					mux.b_user = masters[i].b_user;

					mux.r_valid = masters[i].r_valid;
					mux.b_valid = masters[i].b_valid;
					mux.w_ready = masters[i].w_ready;
				end
			end
		end

		for(i = 0; i < 2; i++) begin
			always_comb begin
				masters[i].ar_valid = state == READ  && selectMaster == i ? mux.ar_valid : 1'b0;
				masters[i].aw_valid = state == WRITE && selectMaster == i ? mux.aw_valid : 1'b0;

				masters[i].ar_addr = mux.ar_addr;
				masters[i].ar_burst = mux.ar_burst;
				masters[i].ar_cache = mux.ar_cache;
				masters[i].ar_id = mux.ar_id;
				masters[i].ar_len = mux.ar_len;
				masters[i].ar_lock = mux.ar_lock;
				masters[i].ar_prot = mux.ar_prot;
				masters[i].ar_qos = mux.ar_qos;
				masters[i].ar_region = mux.ar_region;
				masters[i].ar_size = mux.ar_size;
				masters[i].ar_user = mux.ar_user;

				masters[i].aw_addr = mux.aw_addr;
				masters[i].aw_burst = mux.aw_burst;
				masters[i].aw_cache = mux.aw_cache;
				masters[i].aw_id =  mux.aw_id;
				masters[i].aw_len = mux.aw_len;
				masters[i].aw_lock = mux.aw_lock;
				masters[i].aw_prot = mux.aw_prot;
				masters[i].aw_qos = mux.aw_qos;
				masters[i].aw_region = mux.aw_region;
				masters[i].aw_size = mux.aw_size;
				masters[i].aw_user = mux.aw_user;

				masters[i].w_data = mux.w_data;
				masters[i].w_last = mux.w_last;
				masters[i].w_strb = mux.w_strb;
				masters[i].w_user = mux.w_user;

				masters[i].r_ready = state == READ  && selectMaster == i ? mux.r_ready : 1'b0;
				masters[i].b_ready = state == WRITE && selectMaster == i ? mux.b_ready : 1'b0;
				masters[i].w_valid = state == WRITE && selectMaster == i ? mux.w_valid : 1'b0;
			end
		end

		for(i = 0; i < 2; i++) begin
			always_comb begin
				slaves[i].ar_ready = state == READ  && selectSlave == i ? mux.ar_ready : 1'b0;
				slaves[i].aw_ready = state == WRITE && selectSlave == i ? mux.aw_ready : 1'b0;

				slaves[i].r_data = mux.r_data;
				slaves[i].r_id = mux.r_id[AXI_ID_WIDTH-1:0];
				slaves[i].r_last = mux.r_last;
				slaves[i].r_resp = mux.r_resp;
				slaves[i].r_user = mux.r_user;

				slaves[i].b_id = mux.b_id[AXI_ID_WIDTH-1:0];
				slaves[i].b_resp = mux.b_resp;
				slaves[i].b_user = mux.b_user;

				slaves[i].r_valid = state == READ && selectSlave == i ? mux.r_valid : 1'b0;
				slaves[i].b_valid = state == WRITE && selectSlave == i ? mux.b_valid : 1'b0;
				slaves[i].w_ready = state == WRITE && selectSlave == i ? mux.w_ready : 1'b0;
			end
		end
	endgenerate

	always_ff @(posedge clk) begin
		if (~rst_n) begin
			state = IDLE;
			selectSlave = 0;
			selectMaster = 0;
		end else begin
			case (state)
				IDLE: begin
					if ((slaves[0].ar_valid && slaves[1].ar_valid && selectSlave == 1) ||
							(slaves[0].ar_valid && ~slaves[1].ar_valid)) begin
						selectSlave = 0;
						state = READ;
						if (slaves[0].ar_addr >= regions_low[0] && slaves[0].ar_addr <= regions_high[0]) begin
							selectMaster = 0;
						end else if (slaves[0].ar_addr >= regions_low[1] && slaves[0].ar_addr <= regions_high[1]) begin
							selectMaster = 1;
						end
					end
					else if ((slaves[0].ar_valid && slaves[1].ar_valid && selectSlave == 0) ||
							(~slaves[0].ar_valid && slaves[1].ar_valid)) begin
						//slaves[1].ar_ready = 1'b1;
						selectSlave = 1;
						state = READ;
						if (slaves[1].ar_addr >= regions_low[1] && slaves[1].ar_addr <= regions_high[0]) begin
							selectMaster = 0;
						end else if (slaves[1].ar_addr >= regions_low[1] && slaves[1].ar_addr <= regions_high[1]) begin
							selectMaster = 1;
						end
					end
					else if ((slaves[0].aw_valid && slaves[1].aw_valid && selectSlave == 1) ||
							(slaves[0].aw_valid && ~slaves[1].aw_valid)) begin
						selectSlave = 0;
						state = WRITE;
						if (slaves[0].aw_addr >= regions_low[0] && slaves[0].aw_addr <= regions_high[0]) begin
							selectMaster = 0;
						end else if (slaves[0].aw_addr >= regions_low[1] && slaves[0].aw_addr <= regions_high[1]) begin
							selectMaster = 1;
						end
					end
					else if ((slaves[0].aw_valid && slaves[1].aw_valid && selectSlave == 0) ||
							(~slaves[0].aw_valid && slaves[1].aw_valid)) begin
						selectSlave = 1;
						state = WRITE;
						if (slaves[1].aw_addr >= regions_low[1] && slaves[1].aw_addr <= regions_high[0]) begin
							selectMaster = 0;
						end else if (slaves[1].aw_addr >= regions_low[1] && slaves[1].aw_addr <= regions_high[1]) begin
							selectMaster = 1;
						end
					end
				end
				READ: begin
					if (selectSlave == 0) begin
						if ((selectMaster == 0 && masters[0].r_last && masters[0].r_valid && slaves[0].r_ready) ||
								(selectMaster == 1 && masters[1].r_last && masters[1].r_valid && slaves[0].r_ready)) begin
							state = IDLE;
						end
					end
					else if (selectSlave == 1) begin
						if ((selectMaster == 0 && masters[0].r_last && masters[0].r_valid && slaves[1].r_ready) ||
								(selectMaster == 1 && masters[1].r_last && masters[1].r_valid && slaves[1].r_ready)) begin
							state = IDLE;
						end
					end
				end
				WRITE: begin
					if (selectSlave == 0) begin
						if (selectMaster == 0 && masters[0].b_valid && slaves[0].b_ready ||
								selectMaster == 1 && masters[1].b_valid && slaves[0].b_ready) begin
							state = IDLE;
						end
					end
					else if (selectSlave == 1) begin
						if (selectMaster == 0 && masters[0].b_valid && slaves[1].b_ready ||
								selectMaster == 1 && masters[1].b_valid && slaves[1].b_ready) begin
							state = IDLE;
						end
					end
				end
			endcase
		end
	end

endmodule
